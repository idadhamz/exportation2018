<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class General {

    var $ci;

    public function __construct() {
        $this->ci = &get_instance();
//        $this->isLogin();
    }

    public function cek(){
        
    }
    public function isLogin() {
        if ($this->ci->session->userdata('is_login') == TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // public function cekUserLogin() {
    //     if ($this->isLogin() != TRUE) {
    //         $this->ci->session->set_flashdata('message', 'Anda tidak memiliki hak akses');
    //         redirect('users/login');
    //     }
    // }

    public function cekAdminLogin() {
        if ($this->isLogin() == TRUE) {
            if ($this->ci->session->userdata('level') != 'a3f652c0-f323-11e6-830d-206a8a0a' ) {
                $this->ci->session->set_flashdata('message', 'Anda tidak memiliki hak akses halaman Administrator');
                redirect(base_url());
            }
        } 
        else {
            $this->ci->session->set_flashdata('message', 'Anda tidak memiliki hak akses halaman Administrator');
            redirect(base_url());
        }
    }

    public function cekKasirLogin() {
        if ($this->isLogin() == TRUE) {
            if ($this->ci->session->userdata('level') != 'a3f67939-f323-11e6-830d-206a8a0a' ) {
                $this->ci->session->set_flashdata('message', 'Anda tidak memiliki hak akses halaman Kasir');
                redirect(base_url());
            }
        } 
        else {
            $this->ci->session->set_flashdata('message', 'Anda tidak memiliki hak akses halaman Administrator');
            redirect(base_url());
        }
    }

    public function cekPetugasLogin() {
        if ($this->isLogin() == TRUE) {
            if ($this->ci->session->userdata('level') != 'ab782d6b-f323-11e6-830d-206a8a0a' ) {
                $this->ci->session->set_flashdata('message', 'Anda tidak memiliki hak akses halaman Petugas');
                redirect(base_url());
            }
        } 
        else {
            $this->ci->session->set_flashdata('message', 'Anda tidak memiliki hak akses halaman Administrator');
            redirect(base_url());
        }
    }
}

?>
