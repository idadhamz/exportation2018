<!DOCTYPE html>
<html>
    
<!-- Mirrored from moltran.coderthemes.com/menu_2/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 08 Jun 2018 04:18:42 GMT -->
<head>
        <meta charset="utf-8" />
        <title>HIMSI Exportation 2018</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="<?php echo site_url().'assets/img/HIMSI.png' ?>">

        <?=css('datatables/jquery.dataTables.min.css')?>
        <?=css('datatables/responsive.bootstrap.min.css')?>
        <?=css('datatables/buttons.bootstrap.min.css')?>
        <?=css('datatables/fixedHeader.bootstrap.min.css')?>
        <?=css('datatables/scroller.bootstrap.min.css')?>

        <?=css('bootstrap.min.css')?>
        <?=css('core.css')?>
        <?=css('icons.css')?>
        <?=css('components.css')?>
        <?=css('pages.css')?>
        <?=css('menu.css')?>
        <?=css('responsive.css')?>
        <?=css('select2.min.css')?>


        <?=js('modernizr.min.js')?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

    </head>


    <body>

        <!-- Login -->
        <div class="wrapper-page">
            <div class="panel panel-color panel-primary panel-pages">
                <div class="panel-heading bg-img"> 
                    <div class="bg-overlay"></div>
                    <h3 class="text-center m-t-10 text-white"> Sign In to <strong>Exportation 2018! Admin</strong> </h3>
                </div> 


                <div class="panel-body">
                <?php echo form_open($formaction, 'id="frmlogin" class="form-horizontal"');?>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <?php echo form_input('username','','placeholder="Username" id="username" class="form-control input-md" required = "required"')?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12">
                          <div class="input-group">
                            <span class="input-group-addon"><i class="ion ion-key"></i></span>
                            <?php echo form_password('password','','placeholder="Password" id="password" class="form-control input-md" required = "required"')?>
                            <span class="input-group-btn">
                                <button type="button" class="btn waves-effect waves-light btn-primary" id="show">
                                    <i class="fa fa-eye"></i>
                                </button>
                            </span>
                          </div>
                        </div>
                    </div>
                    
                    <div class="form-group m-t-30">
                        <div class="col-xs-12">
                            <button class="btn btn-block btn-md btn-primary waves-effect waves-light" type="submit" id="submit">Login</button>
                        </div>
                    </div>
                <?php echo form_close(); ?>
                </div>                                 
                
            </div>
        </div>
        <!-- End Navigation Bar-->
        <!-- jQuery  -->
        <?=js('jquery.min.js')?>
        <?=js('bootstrap.min.js')?>
        <?=js('detect.js')?>
        <?=js('fastclick.js')?>
        <?=js('jquery.blockUI.js')?>
        <?=js('waves.js')?>
        <?=js('wow.min.js')?>
        <?=js('jquery.nicescroll.js')?>
        <?=js('jquery.scrollTo.min.js')?>

        <!-- Datatable -->
        <?=js('datatables/jquery.dataTables.min.js') ?>
        <?=js('datatables/dataTables.bootstrap.js') ?>
        <?=js('datatables/dataTables.buttons.min.js') ?>
        <?=js('datatables/buttons.bootstrap.min.js') ?>
        <?=js('datatables/jszip.min.js') ?>
        <?=js('datatables/pdfmake.min.js') ?>
        <?=js('datatables/vfs_fonts.js') ?>
        <?=js('datatables/buttons.html5.min.js') ?>
        <?=js('datatables/buttons.print.min.js') ?>
        <?=js('datatables/dataTables.fixedHeader.min.js') ?>
        <?=js('datatables/dataTables.keyTable.min.js') ?>
        <?=js('datatables/dataTables.responsive.min.js') ?>
        <?=js('datatables/responsive.bootstrap.min.js') ?>
        <?=js('datatables/dataTables.scroller.min.js') ?>
        <?=js('datatables/datatables.init.js') ?>
        
        <!-- App js -->
        <?=js('jquery.app.js')?>

        <!-- moment js  -->
        <?=js('moment.js')?>

        <!-- counters  -->
        <?=js('jquery.waypoints.js')?>
        <?=js('jquery.counterup.min.js')?>

        <!-- dashboard  -->
        <?=js('jquery.dashboard.js')?>

        <script>
        $("#show").click(function() {
          if ($("#password").attr("type") == "password") {
            $("#password").attr("type", "text");

          } else {
            $("#password").attr("type", "password");
          }
        });
        </script>

    </body>

<!-- Mirrored from moltran.coderthemes.com/menu_2/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 08 Jun 2018 04:19:20 GMT -->
</html>