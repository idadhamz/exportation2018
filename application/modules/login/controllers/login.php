<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MX_Controller {

	private $_module;

	public function __construct() {
        parent::__construct();

        //set Module location
        $this->_module = 'login/login';
        $this->load->model('model_login','lm');
        
        // set library
        $this->load->helper('form');
	    $this->load->library('form_validation');
	    $this->load->library('session');
 
    }

	public function admin() {
		$data['formaction']	= $this->_module.'/adminproses';
		$this->load->view($this->_module.'/admin',$data);
	}

	public function adminproses(){
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		if ($this->form_validation->run() == FALSE) {
			redirect(base_url('/login/admin'));
		}
		$uname 	= $this->input->post('username');
		$psw 	= $this->input->post('password');
		$result = $this->lm->cekLogin($uname,$psw);

			if (!empty($result)) {
                $sessionData['id_user'] = $result['id_user'];
                $sessionData['uname'] = $result['username'];
                $sessionData['nama'] = $result['nama'];
                $sessionData['is_login'] = TRUE;

                $this->session->set_userdata($sessionData);
                redirect(base_url('/dashboard/dashboard'));	
            }else {
				echo "<script>
	  					alert('username / Password yang anda masukkan salah');
						window.location.href='".base_url('/login/admin').
					  "'</script>";
					  
			}
        }

	public function logout(){
		if ($this->session->has_userdata('uname') == true) {
			$a = $this->session->sess_destroy();
				redirect(base_url('/login/admin'));
		}
  }
}

?>
