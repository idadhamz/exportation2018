<?php

class dash_model extends CI_Model{

    public function countPeserta(){
        $this->db->select('tbl_peserta.id_peserta');
        $this->db->from('tbl_peserta');
        $this->db->where('tbl_peserta.status_bayar', 1);
        return $this->db->get();
    }
    public function countKegiatan(){
        $this->db->select('tbl_kegiatan.id_kegiatan');
        $this->db->from('tbl_kegiatan');
        $this->db->where('tbl_kegiatan.dlt',NULL);
        return $this->db->get();
    }
    public function countSeminar(){
        $this->db->select('vw_seminar.id_peserta');
        $this->db->from('vw_seminar');
        return $this->db->get();
    }
    public function countWorkshop(){
        $this->db->select('vw_workshop.id_peserta');
        $this->db->from('vw_workshop');
        return $this->db->get();
    }
}