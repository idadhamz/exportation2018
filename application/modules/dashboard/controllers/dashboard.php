<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MX_Controller {
	public function __construct(){
		parent:: __construct();
		$this->_module = 'dashboard/dashboard';
		$this->_header = 'layout/header';

        /*if($this->session->has_userdata('level') !== 'a3f652c0-f323-11e6-830d-206a8a0a'){
                $this->_base = base_url();
                echo "<script>
                        window.location.href='".$this->_base."';
                        </script>";
        }*/      
                $this->load->model('dash_model', 'dash');
	}

        public function index(){
                if($this->session->has_userdata('uname') == FALSE){
                        redirect(base_url());
                }else{
                        $data = array(
                                'jml_peserta' => $this->dash->countPeserta()->num_rows(),
                                'jml_kegiatan' => $this->dash->countKegiatan()->num_rows(),
                                'jml_seminar' => $this->dash->countSeminar()->num_rows(),
                                'jml_workshop' => $this->dash->countWorkshop()->num_rows()
                        );
                        
                        $this->load->view($this->_header);
                        $this->load->view($this->_module, $data);
                }
        }  
}

?>