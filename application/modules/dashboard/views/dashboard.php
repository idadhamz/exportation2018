        <div class="wrapper">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Dashboard Admin</h4>
                    </div>
                </div>


                <!-- Start Widget -->
                <div class="row">
                    <div class="col-sm-6 col-lg-3">
                        <div class="mini-stat clearfix bx-shadow bg-info">
                            <span class="mini-stat-icon"><i class="md md-account-child "></i></span>
                            <div class="mini-stat-info text-right">
                                <span class="counter"><?php echo $jml_peserta ?></span>
                                Jumlah Peserta 
                            </div>
                             <div class="tiles-progress">
                                <div class="m-t-20">
                                    <h6 class="text-uppercase text-white m-0">Exportation 2018</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="mini-stat clearfix bg-purple bx-shadow">
                            <span class="mini-stat-icon"><i class="md md-mic"></i></span>
                            <div class="mini-stat-info text-right">
                                <span class="counter"><?php echo $jml_seminar ?></span>
                                Jumlah Peserta Seminar
                            </div>
                            <div class="tiles-progress">
                                <div class="m-t-20">
                                    <h6 class="text-uppercase text-white m-0">Exportation 2018</h6>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-lg-3">
                        <div class="mini-stat clearfix bg-primary bx-shadow">
                            <span class="mini-stat-icon"><i class="ion-settings"></i></span>
                            <div class="mini-stat-info text-right">
                                <span class="counter"><?php echo $jml_workshop ?></span>
                                Total Peserta Workshop
                            </div>
                            <div class="tiles-progress">
                                <div class="m-t-20">
                                    <h6 class="text-uppercase text-white m-0">Exportation 2018</h6>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-lg-3">
                        <div class="mini-stat clearfix bg-success bx-shadow">
                            <span class="mini-stat-icon"><i class="md md-event"></i></span>
                            <div class="mini-stat-info text-right">
                                <span class="counter"><?php echo $jml_kegiatan ?></span>
                                Jumlah Kegiatan
                            </div>
                            <div class="tiles-progress">
                                <div class="m-t-20">
                                    <h6 class="text-uppercase text-white m-0">Exportation 2018</h6>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- End row-->
        </div>
        <footer class="footer text-right" style="padding: 18px 0px 10px 0px">
               <div class="container " style="">
                   <div class="row" >
                       <div class="col-xs-6 " >
                           2018 © - Admin HIMSI Exportation
                       </div>
                       <div class="col-xs-6 ">
                           <p class="pull-right">all rights reserved to  -  HIMSI Exportation, 2018</p>
                       </div>
                   </div>
               </div>
        </footer>