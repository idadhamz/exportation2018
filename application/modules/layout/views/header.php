<!DOCTYPE html>
<html>
    
<!-- Mirrored from moltran.coderthemes.com/menu_2/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 08 Jun 2018 04:18:42 GMT -->
<head>
        <meta charset="utf-8" />
        <title>HIMSI EXPORTATION 2018</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="<?php echo site_url().'assets/img/Expo.jpg' ?>">

        <?=css('datatables/jquery.dataTables.min.css')?>
        <?=css('datatables/responsive.bootstrap.min.css')?>
        <?=css('datatables/buttons.bootstrap.min.css')?>
        <?=css('datatables/fixedHeader.bootstrap.min.css')?>
        <?=css('datatables/scroller.bootstrap.min.css')?>

        <?=css('bootstrap.min.css')?>
        <?=css('core.css')?>
        <?=css('icons.css')?>
        <?=css('components.css')?>
        <?=css('pages.css')?>
        <?=css('menu.css')?>
        <?=css('responsive.css')?>
        <?=css('select2.min.css')?>


        <?=js('modernizr.min.js')?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

    </head>
    <?php if ($this->session->userdata('nama') != null ){
               $a = "";
               $b = "hidden";
            }
            else{
                $a = "hidden";
                $b = "";
            } 
    ?>

    <body>

        <!-- Navigation Bar-->
        <header id="topnav">
            <div class="topbar-main  navbar m-b-0 b-0">
                <div class="container">

                    <!-- LOGO -->
                    <div class="topbar-left">
                    <a href="#" class="logo"><font color="white" ><?=img('LogoExpo.png', array('height' => '48px','style'=>'margin-top:-10px'))?></font><!-- Polisehat.ID --> </a>
                </div>
                    <!-- End Logo container-->


                    <div class="menu-extras">

                        <ul class="nav navbar-nav navbar-right pull-right">
                            <li class="dropdown user-box">
                                    <a href="" class="dropdown-toggle waves-effect waves-light profile" data-toggle="dropdown" aria-expanded="true">
                                        <?=img('users/avatar-1.jpg', array('class' => 'img-circle user-img',  'alt'  => 'user-img','style' => 'margin-right:5px;margin-bottom:5px;'))?>
                                       <b><?php echo $this->session->userdata('nama') !== null ? $this->session->userdata('nama') : "Klik untuk Login" ; ?></b>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="<?php echo $a ?>"><a href="<?php echo base_url().'login/login/logout' ?>"><i class="fa fa-sign-out"></i> Logout</a></li>
                                        <li class="<?php echo $b ?>"><a href="<?php echo base_url('/login/admin') ?>"><i class="fa fa-sign-in pull-right"></i> Log in</a></li>
                                    </ul>
                            </li>
                        </ul>
                        <div class="menu-item">
                            <!-- Mobile menu toggle-->
                            <a class="navbar-toggle">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- End mobile menu toggle-->
                        </div>
                    </div>

                </div>
            </div>

            <div class="navbar-custom">
                <div class="container">
                    <div id="navigation">
                        <!-- Navigation Menu-->
                        <ul class="navigation-menu">
                            <li>
                                <a href="<?php echo base_url().'dashboard/dashboard' ?>"><i class="md md-home"></i> <span> Dashboard </span> </a>
                            </li>

                            <li>
                                <a href="<?php echo base_url().'admin/all_peserta' ?>"><i class="md md-account-child"></i> <span> Seluruh Peserta</span> </a>
                            </li>

                            <li>
                                <a href="<?php echo base_url().'admin/peserta' ?>"><i class="md md-mic"></i> <span> Peserta Seminar</span> </a>
                            </li>

                            <li>
                                <a href="<?php echo base_url().'admin/peserta' ?>"><i class="ion-settings"></i> <span> Peserta Workshop</span> </a>
                            </li>

                            <li>
                                <a href="<?php echo base_url().'admin/jenis_peserta' ?>"><i class="md md-accessibility"></i> <span> Kategori Peserta</span> </a>
                            </li>
                            
                            <li>
                                <a href="<?php echo base_url().'admin/kegiatan' ?>"><i class="md md-event"></i> <span> Data Kegiatan</span> </a>
                            </li>
<!--                             <li class="has-submenu">
                                <a href="#"><i class="md md-invert-colors-on"></i> <span> Divisi </span> </a>
                                <ul class="submenu">
                                    <li><a href="grid.html">Data Divisi Dikti</a></li>
                                    <li><a href="portlets.html">Data Divisi Humas</a></li>
                                    <li><a href="widgets.html">Data Divisi Kesekum</a></li>
                                    <li><a href="nestable-list.html">Data Divisi Social Media</a></li>
                                </ul>
                            </li> -->

                        </ul>
                        <!-- End navigation menu  -->
                    </div>
                </div>
            </div>
        </header>
        <!-- End Navigation Bar-->

        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $('.counter').counterUp({
                    delay: 100,
                    time: 1200
                });
            });
        </script>

    </body>

<!-- Mirrored from moltran.coderthemes.com/menu_2/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 08 Jun 2018 04:19:20 GMT -->
</html>