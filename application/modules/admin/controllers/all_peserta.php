<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class All_peserta extends MX_Controller {
	public function __construct(){
		parent:: __construct();
		$this->_module = 'admin/all_peserta';
		$this->_header = 'layout/header';

        /*if($this->session->has_userdata('level') !== 'a3f652c0-f323-11e6-830d-206a8a0a'){
                $this->_base = base_url();
                echo "<script>
                        window.location.href='".$this->_base."';
                        </script>";
        }*/      
                $this->load->model('model_all_peserta', 'all_peserta');
	}

    public function index(){
        
            if($this->session->has_userdata('uname') == FALSE){
                redirect(base_url());
            }else{
                $data = array();
                
                if($this->all_peserta->getData()->num_rows()>0){
                        $data['all_peserta'] = $this->all_peserta->getData()->result();
                }else{
                        $data['all_peserta'] = new stdClass();
                }

                $this->load->view($this->_header);
                $this->load->view($this->_module.'/all_peserta', $data);
            }
        }

    public function cetak(){

            if($this->session->has_userdata('uname') == FALSE){
                redirect(base_url());
            }else{

                $id_peserta = $this->input->get('id_peserta');
                $nama = $this->input->get('nama');
                $kegiatan = $this->input->get('kegiatan');

                $this->load->library('ciqrcode'); //pemanggilan library QR CODE

                $config['cacheable']    = true; //boolean, the default is true
                $config['cachedir']     = './assets/'; //string, the default is application/cache/
                $config['errorlog']     = './assets/'; //string, the default is application/logs/
                $config['imagedir']     = './assets/img/qrcode_peserta/'; //direktori penyimpanan qr code
                $config['quality']      = true; //boolean, the default is true
                $config['size']         = '1024'; //interger, the default is 1024
                $config['black']        = array(224,255,255); // array, default is array(255,255,255)
                $config['white']        = array(70,130,180); // array, default is array(0,0,0)
                $this->ciqrcode->initialize($config);

                $image_name=$id_peserta.'_'.$nama.'_'.$kegiatan.'.png'; //buat name dari qr code sesuai dengan nim

                $params['data'] = $id_peserta.'_'.$nama.'_'.$kegiatan; //data yang akan di jadikan QR CODE
                $params['level'] = 'H'; //H=High
                $params['size'] = 10;
                $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
                $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE

                if ($this->db->trans_status() === FALSE ) {
                $this->db->trans_rollback();
                echo "<script>
                        alert('Data tidak tersimpan karna kesalahan tertentu ');
                        window.location.href='".base_url()."admin/all_peserta/all_peserta';
                        </script>";
                }
                else {
                    $this->db->trans_commit();
                                    echo "<script>
                                alert('Barcode Berhasil Dibuat! ');
                                window.location.href='".base_url()."admin/all_peserta';
                                </script>";
                }

            }
        }

}

?>