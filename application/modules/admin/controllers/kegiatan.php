<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kegiatan extends MX_Controller {
	public function __construct(){
		parent:: __construct();
		$this->_module = 'admin/kegiatan';
		$this->_header = 'layout/header';

        /*if($this->session->has_userdata('level') !== 'a3f652c0-f323-11e6-830d-206a8a0a'){
                $this->_base = base_url();
                echo "<script>
                        window.location.href='".$this->_base."';
                        </script>";
        }*/      
                $this->load->model('model_kegiatan', 'kegiatan');
	}

        public function index(){
            if($this->session->has_userdata('uname') == FALSE){
                redirect(base_url());
            }else{
                $data = array();
                
                if($this->kegiatan->getData()->num_rows()>0){
                        $data['kegiatan'] = $this->kegiatan->getData()->result();
                }else{
                        $data['kegiatan'] = new stdClass();
                }

                $this->load->view($this->_header);
                $this->load->view($this->_module.'/kegiatan', $data);
            }
        } 

        public function form(){
            if($this->session->has_userdata('uname') == FALSE){
                redirect(base_url());
            }else{

                $id_kegiatan = $this->input->get('id_kegiatan');        
                $data = array(
                    'action' => base_url().'admin/kegiatan/save'
                );

                if($id_kegiatan != ""){
                    $data['default']    = $this->kegiatan->getData($id_kegiatan)->row();
                }
                elseif ($id_kegiatan == "") 
                {
                    $data['default']        = "";
                }

                    $this->load->view($this->_header); 
                    $this->load->view($this->_module.'/form', $data);
            }
        }

        function save(){
            $id_kegiatan = $this->input->post('id_kegiatan');
            $this->db->trans_begin();
            $datakegiatan       = $this->input->post("data");            

            if ($id_kegiatan == "") {
                $this->kegiatan->save_as_new($datakegiatan);
            }
            else if($id_kegiatan != ""){
                $this->kegiatan->save($datakegiatan, $id_kegiatan);
            }

            if ($this->db->trans_status() === FALSE ) {
                $this->db->trans_rollback();
                echo "<script>
                        alert('Data tidak tersimpan karna kesalahan tertentu ');
                        window.location.href='".base_url()."admin/kegiatan/form';
                        </script>";
            }
            else {
                $this->db->trans_commit();
                                echo "<script>
                            alert('Data Berhasil Disimpan ');
                            window.location.href='".base_url()."admin/kegiatan';
                            </script>";
            }
        

        }
}

?>