<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jenis_peserta extends MX_Controller {
	public function __construct(){
		parent:: __construct();
		$this->_module = 'admin/jenis_peserta';
		$this->_header = 'layout/header';

        /*if($this->session->has_userdata('level') !== 'a3f652c0-f323-11e6-830d-206a8a0a'){
                $this->_base = base_url();
                echo "<script>
                        window.location.href='".$this->_base."';
                        </script>";
        }*/      
                $this->load->model('model_jenis', 'jenis');
	}

        public function index(){
            if($this->session->has_userdata('uname') == FALSE){
                redirect(base_url());
            }else{
                $data = array();
                
                if($this->jenis->getData()->num_rows()>0){
                        $data['jenis'] = $this->jenis->getData()->result();
                }else{
                        $data['jenis'] = new stdClass();
                }

                $this->load->view($this->_header);
                $this->load->view($this->_module.'/jenis', $data);
            }
        } 

        public function form(){
            if($this->session->has_userdata('uname') == FALSE){
                redirect(base_url());
            }else{

                $id_kategori = $this->input->get('id_kategori');        
                $data = array(
                    'action' => base_url().'admin/jenis_peserta/save'
                );

                if($id_kategori != ""){
                    $data['default']    = $this->jenis->getData($id_kategori)->row();
                }
                elseif ($id_kategori == "") 
                {
                    $data['default']        = "";
                }

                    $this->load->view($this->_header); 
                    $this->load->view($this->_module.'/form', $data);
            }
        }

        function save(){
            $id_kategori = $this->input->post('id_kategori');
            $this->db->trans_begin();
            $datajenis       = $this->input->post("data");            

            if ($id_kategori == "") {
                $this->jenis->save_as_new($datajenis);
            }
            else if($id_kategori != ""){
                $this->jenis->save($datajenis, $id_kategori);
            }

            if ($this->db->trans_status() === FALSE ) {
                $this->db->trans_rollback();
                echo "<script>
                        alert('Data tidak tersimpan karna kesalahan tertentu ');
                        window.location.href='".base_url()."admin/jenis_peserta/form';
                        </script>";
            }
            else {
                $this->db->trans_commit();
                                echo "<script>
                            alert('Data Berhasil Disimpan ');
                            window.location.href='".base_url()."admin/jenis_peserta';
                            </script>";
            }
        

        }
}

?>