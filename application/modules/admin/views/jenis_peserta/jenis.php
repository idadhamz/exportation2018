        <div class="wrapper">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="md md-accessibility" style="margin-right:10px;"></i> Kategori Peserta</h3>
                            </div>
                            <div class="panel-body">
                                <a href="<?php echo base_url().'admin/jenis_peserta/form' ?>">
                                    <button class="btn btn-info" style="margin-bottom:10px;margin-top:-10px;"> <i class="fa fa-plus m-r-5"></i><span>Tambah Kategori Peserta</span> </button>
                                </a>

                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <table id="jenis_peserta" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th class="text-center" width="5%">No</th>
                                                    <th>Kategori</th>
                                                    <th width="15%">Aksi</th>
                                                </tr>
                                            </thead>


                                            <tbody>
                                                <tr>
                                                <?php $no=0; foreach($jenis as $k){ $no++?>
                                                    <td class="text-center"><?php echo $no ?></td>
                                                    <td><?php echo $k->kategori?></td>
                                                    <td>
                                                        <a href="<?php echo base_url().'admin/jenis_peserta/form?id_kategori='.$k->id_kategori ?>">
                                                            <button class="btn btn-success btn-xs"><span>Edit</span>
                                                            </button>
                                                        </a>
                                                    </td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>   
                </div> <!-- End Row -->
                <footer class="footer text-right" style="padding: 18px 0px 10px 0px">
                               <div class="container " style="">
                                   <div class="row" >
                                       <div class="col-xs-6 " >
                                           2018 © - Admin HIMSI Exportation
                                       </div>
                                       <div class="col-xs-6 ">
                                           <p class="pull-right">all rights reserved to  -  HIMSI Exportation, 2018</p>
                                       </div>
                                   </div>
                               </div>
                </footer>
            </div>
        </div>

         <!-- jQuery  -->
        <?=js('jquery.min.js')?>
        <?=js('bootstrap.min.js')?>
        <?=js('detect.js')?>
        <?=js('fastclick.js')?>
        <?=js('jquery.blockUI.js')?>
        <?=js('waves.js')?>
        <?=js('wow.min.js')?>
        <?=js('jquery.nicescroll.js')?>
        <?=js('jquery.scrollTo.min.js')?>

        <!-- Datatable -->
        <?=js('datatables/jquery.dataTables.min.js') ?>
        <?=js('datatables/dataTables.bootstrap.js') ?>
        <?=js('datatables/dataTables.buttons.min.js') ?>
        <?=js('datatables/buttons.bootstrap.min.js') ?>
        <?=js('datatables/jszip.min.js') ?>
        <?=js('datatables/pdfmake.min.js') ?>
        <?=js('datatables/vfs_fonts.js') ?>
        <?=js('datatables/buttons.html5.min.js') ?>
        <?=js('datatables/buttons.print.min.js') ?>
        <?=js('datatables/dataTables.fixedHeader.min.js') ?>
        <?=js('datatables/dataTables.keyTable.min.js') ?>
        <?=js('datatables/dataTables.responsive.min.js') ?>
        <?=js('datatables/responsive.bootstrap.min.js') ?>
        <?=js('datatables/dataTables.scroller.min.js') ?>
        <?=js('datatables/datatables.init.js') ?>
        
        <!-- App js -->
        <?=js('jquery.app.js')?>

        <!-- moment js  -->
        <?=js('moment.js')?>

        <!-- counters  -->
        <?=js('jquery.waypoints.js')?>
        <?=js('jquery.counterup.min.js')?>

        <!-- dashboard  -->
        <?=js('jquery.dashboard.js')?>
        
        <script>
          $(document).ready(function() {
            $('#jenis_peserta').DataTable();
          });
        </script>