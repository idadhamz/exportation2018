        <div class="wrapper">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title" style="margin-left:8px;">Form input Kegiatan Peserta</h3>
                            </div>
                            <div class="panel-body">
                                <?php 
                                        $hidden = array('id_kegiatan' => !empty($default->id_kegiatan)? $default->id_kegiatan : '',
                                                'class' => 'form-horizontal', 'id' => 'form');
                                        echo form_open_multipart($action,$hidden);
                                ?>
                                    <div class="form-group">
                                        <label for="kategori" class="col-sm-3 control-label">Kode Kegiatan</label>
                                        <div class="col-sm-6">
                                          <?php
                                            $data = array('name' => 'id_kegiatan', 'value' => !empty($default->id_kegiatan)? $default->id_kegiatan : '', 'class' => 'form-control', 'type'=> 'hidden', 'id' => 'id_kegiatan');
                                            echo form_input($data); 
                                            $data = array('name' => 'data[id_kegiatan]', 'value' => !empty($default->id_kegiatan)? $default->id_kegiatan : '', 'class' => 'form-control', 'type'=> 'text', 'id' => 'id_kegiatan', 'placeholder' => 'Kode Kegiatan','required' => 'required');
                                            echo form_input($data); 
                                          ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="kategori" class="col-sm-3 control-label">Nama</label>
                                        <div class="col-sm-6">
                                          <?php 
                                            $data = array('name' => 'data[kegiatan]', 'value' => !empty($default->kegiatan)? $default->kegiatan : '', 'class' => 'form-control', 'type'=> 'text', 'id' => 'kegiatan', 'placeholder' => 'Nama','required' => 'required');
                                            echo form_input($data); 
                                          ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="kategori" class="col-sm-3 control-label">Jenis</label>
                                        <div class="col-sm-6">
                                          <?php 
                                            $data = array('name' => 'data[jenis]', 'value' => !empty($default->jenis)? $default->kegiatan : '', 'class' => 'form-control', 'type'=> 'text', 'id' => 'jenis', 'placeholder' => 'Jenis','required' => 'required');
                                            echo form_input($data); 
                                          ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="kategori" class="col-sm-3 control-label">Tipe Peserta</label>
                                        <div class="col-sm-6">
                                          <?php 
                                            $data = array('name' => 'data[tipe_user]', 'value' => !empty($default->tipe_user)? $default->tipe_user : '', 'class' => 'form-control', 'type'=> 'text', 'id' => 'tipe_user', 'placeholder' => 'Tipe Peserta','required' => 'required');
                                            echo form_input($data); 
                                          ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="kategori" class="col-sm-3 control-label">Harga</label>
                                        <div class="col-sm-6">
                                          <?php 
                                            $data = array('name' => 'data[harga]', 'value' => !empty($default->harga)? $default->harga : '', 'class' => 'form-control', 'type'=> 'number', 'id' => 'harga', 'placeholder' => 'Harga','required' => 'required');
                                            echo form_input($data); 
                                          ?>
                                        </div>
                                    </div>

                                    <div class="form-group m-b-0">
                                        <div class="col-sm-offset-3 col-sm-9">
                                            <button type="submit" class="btn btn-success waves-effect waves-light">Submit</button>
                                            <a href="<?php echo base_url().'admin/kegiatan' ?>" class="btn btn-primary waves-effect waves-light">Batal</a>
                                        </div>
                                    </div>
                                <?php echo form_close() ?>
                            </div> <!-- panel-body -->
                        </div>
                    </div>   
                </div> <!-- End Row -->
            </div>
        </div>
        <footer class="footer text-right" style="padding: 18px 0px 10px 0px">
               <div class="container " style="">
                   <div class="row" >
                       <div class="col-xs-6 " >
                           2018 © - Admin HIMSI Exportation
                       </div>
                       <div class="col-xs-6 ">
                           <p class="pull-right">all rights reserved to  -  HIMSI Exportation, 2018</p>
                       </div>
                   </div>
               </div>
        </footer>

         <!-- jQuery  -->
        <?=js('jquery.min.js')?>
        <?=js('bootstrap.min.js')?>
        <?=js('detect.js')?>
        <?=js('fastclick.js')?>
        <?=js('jquery.blockUI.js')?>
        <?=js('waves.js')?>
        <?=js('wow.min.js')?>
        <?=js('jquery.nicescroll.js')?>
        <?=js('jquery.scrollTo.min.js')?>

        <!-- Datatable -->
        <?=js('datatables/jquery.dataTables.min.js') ?>
        <?=js('datatables/dataTables.bootstrap.js') ?>
        <?=js('datatables/dataTables.buttons.min.js') ?>
        <?=js('datatables/buttons.bootstrap.min.js') ?>
        <?=js('datatables/jszip.min.js') ?>
        <?=js('datatables/pdfmake.min.js') ?>
        <?=js('datatables/vfs_fonts.js') ?>
        <?=js('datatables/buttons.html5.min.js') ?>
        <?=js('datatables/buttons.print.min.js') ?>
        <?=js('datatables/dataTables.fixedHeader.min.js') ?>
        <?=js('datatables/dataTables.keyTable.min.js') ?>
        <?=js('datatables/dataTables.responsive.min.js') ?>
        <?=js('datatables/responsive.bootstrap.min.js') ?>
        <?=js('datatables/dataTables.scroller.min.js') ?>
        <?=js('datatables/datatables.init.js') ?>
        
        <!-- App js -->
        <?=js('jquery.app.js')?>

        <!-- moment js  -->
        <?=js('moment.js')?>

        <!-- counters  -->
        <?=js('jquery.waypoints.js')?>
        <?=js('jquery.counterup.min.js')?>

        <!-- dashboard  -->
        <?=js('jquery.dashboard.js')?>

        <!--form validation-->
        <?=js('jquery-validation/dist/jquery.validate.min.js') ?>

        <!--form validation init-->
        <?=js('jquery-validation/form-validation-init.js') ?>

        <!-- parsley  -->
        <?=js('parsley/parsley.js') ?>
        <?=js('parsley/parsley.min.js') ?>

        <script>

            $(document).ready(function() {
                  $('#form').parsley();
            });

            $('#conpas').val($('#password').val());

        </script>