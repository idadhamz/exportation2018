<?php

class model_kegiatan extends CI_Model{
    private $_table           = 'tbl_kegiatan';
    protected $primary_key  = 'id_kegiatan';

    public function save_as_new($data) {
        $this->db->set('tbl_kegiatan.crt', 'NOW()', FALSE);
        $this->db->insert($this->_table, $data);
    }

    // untuk update data
    public function save($data, $key) {
        $this->db->set('tbl_kegiatan.mdf', 'NOW()', FALSE);
        $this->db->update($this->_table, $data, array($this->primary_key => $key));
    }


    // untuk update data
    public function delete($data, $key) {
        $this->db->set('tbl_kegiatan.dlt', 'NOW()', FALSE);
        $this->db->update($this->_table, $data,array($this->primary_key => $key));
    }

    public function getData($key    = ""){
        $this->db->select('tbl_kegiatan.*');
        $this->db->from('tbl_kegiatan');
        $this->db->where('tbl_kegiatan.dlt', NULL, TRUE);

        if($key != "")
            $this->db->where($this->primary_key,$key);


        return $this->db->get();
    }
}