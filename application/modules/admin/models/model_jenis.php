<?php

class model_jenis extends CI_Model{
    private $_table           = 'tbl_kategori';
    protected $primary_key  = 'id_kategori';

    public function save_as_new($data) {
        $this->db->set('tbl_kategori.crt', 'NOW()', FALSE);
        $this->db->insert($this->_table, $data);
    }

    // untuk update data
    public function save($data, $key) {
        $this->db->set('tbl_kategori.mdf', 'NOW()', FALSE);
        $this->db->update($this->_table, $data, array($this->primary_key => $key));
    }


    // untuk update data
    public function delete($data, $key) {
        $this->db->set('tbl_kategori.dlt', 'NOW()', FALSE);
        $this->db->update($this->_table, $data,array($this->primary_key => $key));
    }

    public function getData($key    = ""){
        $this->db->select('tbl_kategori.*');
        $this->db->from('tbl_kategori');
        $this->db->where('tbl_kategori.dlt', NULL, TRUE);

        if($key != "")
            $this->db->where($this->primary_key,$key);


        return $this->db->get();
    }
}