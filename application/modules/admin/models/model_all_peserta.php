<?php

class model_all_peserta extends CI_Model{
    private $_table           = 'tbl_peserta';
    protected $primary_key  = 'id_peserta';

    public function getData($key    = ""){
        $this->db->select('tbl_peserta.*, tbl_kategori.kategori, tbl_kegiatan.kegiatan');
        $this->db->from('tbl_peserta');
        $this->db->where('tbl_peserta.status_bayar', 1);
        $this->db->join('tbl_kategori', 'tbl_kategori.id_kategori = tbl_peserta.id_kategori');
        $this->db->join('tbl_kegiatan', 'tbl_kegiatan.id_kegiatan = tbl_peserta.id_kegiatan');

        if($key != "")
            $this->db->where($this->primary_key,$key);


        return $this->db->get();
    }
}